# pylint: disable=missing-module-docstring
# Python Operators are used to perform operations on variables and values.
# pylint: disable=C0103
# pylint: disable=W0621

def addition(a, b):
    """Addition"""
    total = a + b
    return total


def subtraction(a, b):
    """subtraction"""
    total = a - b
    return total


def multiplication(a, b):
    """multiplication"""
    total = a * b
    return total


def division(a, b):
    """division"""""
    total = a/b
    return total


a = 10
b = 5

add_func = (addition(a, b))
sub_func = (subtraction(a, b))
times_func = (multiplication(a, b))
div_func = (division(a, b))

print(f"The value of {a} + {b} is {add_func}")
print(f"The value of {a} - {b} is {sub_func}")
print(f"The value of {a} * {b} is {times_func}")
print(f"The value of {a} / {b} is {div_func}")

# CHALLENGE
# Can you use other Python Operators and if
# statements to write a function that calculates BMI (bmi = weight / height^2)

# if bmi <= 18.5 return "Underweight"

# if bmi <= 25.0 return "Normal"

# if bmi <= 30.0 return "Overweight"

# if bmi > 30 return "Obese"

# height = float(input("Enter your height in cm: "))
# weight = float(input("Enter your weight in kg: "))

# BMI = weight/(height/100)**2
# print(f"You BMI is {BMI}")

# if BMI <= 18.5:
#     print("You are underweight")
# elif BMI <= 25.0:
#     print("You are normal")
# elif BMI <= 30.0:
#     print("You are overweight")
# else:
#     print("you are obese")
