# S3 bucket 
resource "aws_s3_bucket" "sapna-tf-test-bucket" {
  bucket = "sapna-tf-test-bucket"

  tags = {
    Name        = "sapna bucket"
    Environment = "Dev"
  }
}

# ECR resource
resource "aws_ecr_repository" "foo" {
  name                 = "bar"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

# IAM role

resource "aws_iam_role" "sapna-tf-test_role" {
  name = "sapna-tf-test_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}
