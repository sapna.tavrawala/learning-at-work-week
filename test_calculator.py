""""DOCSTRING"""
# pylint: disable=C0103
from calculator import addition, subtraction, multiplication, division


def test_addition():
    """Addition"""
    x = addition(1, 1)
    assert x == 2


def test_subtraction():
    """Subtraction"""
    x = subtraction(1, 1)
    assert x == 0


def test_multiplcation():
    """Multiplcation"""
    x = multiplication(1, 1)
    assert x == 1


def test_division():
    """Division"""
    x = division(1, 1)
    assert x == 1
